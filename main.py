from event_reminder import EventReminder
from notification_info import NotificationInfo
from pushover_notification import PushOverNotificationSender
from event_reminder_GUI_elements import EventReminderGUIElements
from PyQt5.QtWidgets import QApplication
import sys

if __name__ == '__main__':
    app = QApplication(sys.argv)

    # Create EventReminder and DateHandler instances (replace these lines as needed)
    # Initialize DateHandler and EventReminder
    notification_info_holder = NotificationInfo()
    notification_sender = PushOverNotificationSender("config.json")
    event_reminder = EventReminder(notification_sender, notification_info_holder, notification_sender.json_file_handler)
    event_reminder.check_for_notifications()
    ex = EventReminderGUIElements(event_reminder)
    ex.setWindowTitle('Event Reminder')
    ex.resize(300, 200)
    ex.show()

    sys.exit(app.exec_())