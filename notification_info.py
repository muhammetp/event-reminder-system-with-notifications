from datetime import datetime
from typing import Dict, List
from enum import Enum, auto

CONFIG_DATE_FORMAT = "%m/%d/%Y"
class NotificationStatus(Enum):
    SENT = auto()
    NOT_SENT = auto()
    ACK = auto()
    DONE = auto()


class NotificationInfo():
    due_dates: Dict[str, datetime]
    notification_periods: Dict[str, List[int]]
    notification_status: Dict[str, NotificationStatus]

    def __init__(self):
        self.due_dates = {}
        self.notification_periods = {}
        self.notification_status = {}

    @staticmethod
    def _parse_info_data_from_dict(info_val_dict: dict):
        due_date = datetime.strptime(info_val_dict["due_date"], CONFIG_DATE_FORMAT)
        notification_status = NotificationStatus[info_val_dict["notification_status"]]
        return due_date, info_val_dict["notification_period"], notification_status

    def set_init_info(self, info_dict: dict):
        for event_name, info_val_dict in info_dict.items():
            due_date, notification_period, notification_status = self._parse_info_data_from_dict(info_val_dict)
            self.add_new_event_info(event_name, due_date, notification_period, notification_status)

    # function

    def add_new_event_info(self, event_name: str, due_date: datetime, notification_period: List[int],
                           notification_status=NotificationStatus.NOT_SENT):
        try:
            self.due_dates[event_name] = due_date
            self.notification_periods[event_name] = notification_period
            self.notification_status[event_name] = notification_status
            return True
        except Exception as e:
            print(f"Error adding new event info: {e}")
            return False

    def remove_event_info(self, event_name:str):
        try:
            del self.due_dates[event_name]
            del self.notification_periods[event_name]
            del self.notification_status[event_name]
            return True
        except KeyError:
            print(f"The key {event_name} does not exist in notification info!")
            return False