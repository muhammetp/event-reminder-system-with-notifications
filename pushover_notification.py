import requests
from notification_sender import NotificationSender
from json_file_handler import JSONFileHandler
import logging

class PushOverNotificationSender(NotificationSender):
    api_key: str
    user_key: str

    def __init__(self, json_file_path: str):
        self.json_file_handler = JSONFileHandler(json_file_path)
        self.api_key = self.json_file_handler.read_by_key("token")
        self.user_key = self.json_file_handler.read_by_key("user_key")

    def send_notification(self, message)->bool:
        """
        Sends a notification using the Pushover API.
        Returns True if sent successfully, False otherwise.
        """
        b_sent = False
        if not self.api_key or not self.user_key:
            logging.error(f"api_key or user_key not exist")
            return b_sent
        payload = {
            "token": self.api_key, "user": self.user_key, "message": message}
        # files = include_audio()
        try:
            r = requests.post("https://api.pushover.net/1/messages.json", data=payload)

            if r.status_code == 200:
                logging.info("Notification sent successfully.")
                b_sent = True
            else:
                logging.error(f"Failed to send notification. Status code: {r.status_code}")

        except requests.RequestException as e:
            logging.error(f"Error sending notification: {e}")
        return b_sent

if __name__ == "__main__":
    pushover_handler = PushOverNotificationSender("config_uama.json")
    pushover_handler.send_notification(message="pushover deneme")
