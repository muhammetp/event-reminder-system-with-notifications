import tkinter as tk
from tkinter import ttk
from datetime import datetime, timedelta
from event_reminder import EventReminder


class TKGuiHandler:
    event_reminder = EventReminder
    event_name_entry = ttk.Entry
    due_date_entry = ttk.Entry
    submit_button = ttk.Button
    check_button = ttk.Button
    def __init__(self, event_reminder:EventReminder):
        self.event_reminder = event_reminder
        # GUI code

        self.init_gui_elements()
    def init_gui_elements(self):
        root = tk.Tk()
        root.title("Event Reminder")
        ttk.Label(root, text="Event Name:").grid(row=0, column=0)
        event_name_entry = ttk.Entry(root)
        event_name_entry.grid(row=0, column=1)
        self.event_name_entry = event_name_entry

        ttk.Label(root, text="Due Date (YYYY-MM-DD):").grid(row=1, column=0)
        due_date_entry = ttk.Entry(root)
        due_date_entry.grid(row=1, column=1)
        initial_due_date = datetime.now().strftime('%Y-%m-%d')
        due_date_entry.insert(0, initial_due_date)
        self.due_date_entry = due_date_entry

        submit_button = ttk.Button(root, text="Add Event", command=self.submit_event)
        submit_button.grid(row=2, column=0)
        self.submit_button = submit_button

        check_button = ttk.Button(root, text="Check Notifications", command=self.check_notifications)
        check_button.grid(row=2, column=1)
        self.check_button = check_button

        result_label = ttk.Label(root, text="")
        result_label.grid(row=3, columnspan=2)
        self.result_label = result_label

        root.mainloop()

    def submit_event(self):
        event_name = self.event_name_entry.get()
        due_date_str = self.due_date_entry.get()

        try:
            due_date = datetime.strptime(due_date_str, '%Y-%m-%d')
        except ValueError:
            self.result_label.config(text="Invalid date format. Use YYYY-MM-DD")
            return

        self.event_reminder.add_event_reminder(event_name, due_date)
        self.result_label.config(text=f"Event {event_name} added with due date {due_date}")


    def check_notifications(self):
        self.event_reminder.check_for_notifications()
        self.result_label.config(text="Checked notifications. See console for details.")







