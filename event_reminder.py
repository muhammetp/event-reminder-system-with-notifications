from datetime import datetime, timedelta
from typing import Dict, List
from notification_sender import NotificationSender
from notification_info import NotificationInfo, NotificationStatus, CONFIG_DATE_FORMAT
from pushover_notification import PushOverNotificationSender
from json_file_handler import JSONFileHandler
from pathlib import Path


class EventReminder:
    notification_info = NotificationInfo
    notification_sender = NotificationSender

    def __init__(self, notification_sender: NotificationSender, notification_info: NotificationInfo,
                 json_file_handler: JSONFileHandler):
        self.notification_sender = notification_sender
        self.notification_info = notification_info
        self.json_file_handler = json_file_handler
        info_json_path = json_file_handler.info_file_path
        self.get_init_info(info_json_path)

    def get_init_info(self, info_json_path: Path):
        try:
            info_data = self.json_file_handler.read_all(info_json_path)
            if not info_data:
                print("Warning: No info data found in JSON.")
                return
            self.notification_info.set_init_info(info_data)
            self.remove_expired_init_task(info_json_path)
        except Exception as e:
            print(f"Error getting init notification info from JSON: {e}")

    def remove_expired_init_task(self, info_json_path: Path):
        self.json_file_handler.flush_file(info_json_path)
        cur_date = datetime.now()
        not_info_data = self.notification_info
        for event_name, due_date in not_info_data.due_dates.items():
            if due_date >= cur_date:
                notification_periods = not_info_data.notification_periods[event_name]
                notification_status = not_info_data.notification_status[event_name]
                self._write_event_into_json(event_name, due_date, notification_periods, notification_status)

    def _write_event_into_json(self, event_name: str, due_date: datetime, notification_period: list,
                               notification_status = NotificationStatus.NOT_SENT):
        """Whenever you're writing to a file, there's a potential for error."""
        try:
            val_dict = {'due_date': due_date.strftime(CONFIG_DATE_FORMAT), 'notification_period': notification_period,
                        'notification_status': notification_status.name}
            self.json_file_handler.write(event_name, val_dict)
            return True
        except Exception as e:
            print(f"Error writing event to JSON: {e}")
            return False
    def add_event_reminder(self, event_name: str, due_date: datetime, notification_period: List[int] = [7]):
        b_add_new_event = False
        if self.notification_info.add_new_event_info(event_name, due_date, notification_period):
            b_add_new_event = self._write_event_into_json(event_name, due_date, notification_period)
        return b_add_new_event

    def remove_event_reminder(self, event_name: str):
        """Whenever you're writing to a file, there's a potential for error."""
        try:
            if event_name in self.notification_info.due_dates:
                if self.notification_info.remove_event_info(event_name):
                    return self.json_file_handler.remove_by_key(event_name)
        except Exception as e:
            print(f"Error removing event reminder for {event_name}: {e}")
            return False

    def check_for_notifications(self):
        try:
            current_date = datetime.now()
            message = None
            for event_name, due_date in self.notification_info.due_dates.items():
                notification_periods = self.notification_info.notification_periods[event_name]
                day_delta = (due_date - current_date).days
                if day_delta < 0:
                    continue
                for notification_period in notification_periods:
                    if day_delta <= notification_period and \
                            self.notification_info.notification_status[event_name] != NotificationStatus.ACK:
                        message = f"Reminder: {event_name} is due on {due_date}."
                        self.notification_sender.send_notification(message)
                        break
        except Exception as e:
            print(f"Error checking for notifications: {e}")
        return message


if __name__ == "__main__":
    # Create instances of DateHandler and MailHandler
    notification_info = NotificationInfo()
    notification_handler = PushOverNotificationSender("config.json")

    # Create the main EventReminder object
    reminder = EventReminder(notification_handler, notification_info)
    deadline_date = datetime.now() + timedelta(days=8)
    reminder.add_event_reminder("uama_bill_notification", deadline_date)
    reminder.add_event_reminder("uama_bill_notification2", deadline_date)
    reminder.check_for_notifications()
