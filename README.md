# Event Reminder System with Notifications

This Event Reminder System is built in Python and includes various components for adding event reminders, managing due dates, and sending notifications through different channels like Email and Pushover.

## Table of Contents
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Classes](#classes)
  - [NotificationInfo](#notificationinfo)
  - [NotificationSender](#notificationsender)
  - [MailHandler (Under Development)](#mailhandler)
  - [PushOverNotificationSender](#pushovernotificationsender)
  - [EventReminder](#eventreminder)
  - [EventReminderGUIElements](#eventreminderguielements) 
  - [JSONFileHandler](#jsonfilehandler) 
- [Usage](#usage)
- [Scheduler Settings](#scheduler-settings)
## Installation

1. Clone the repository:
2. create an account by following steps in [how to use it](https://support.pushover.net/i7-what-is-pushover-and-how-do-i-use-it)
3. install [pushover app](https://play.google.com/store/apps/details?id=net.superblock.pushover)
4. 
```bash
   pip install requests
   pip install tk
```
5. create a json file as follows:
```
"token": "xxxx",
"user_key": "xxxxxx",
"notifications_from_emails": "xxxxxx.net"
```
6. then insert this json file into where main script is located to run it directly
## Features

- Add and remove event reminders
- Notifications via multiple channels
- Simple CLI interface

## Requirements

- Python 3.x
- `requests` library for Pushover notifications
- A Pushover account for Pushover notifications
- A Mail server for email notifications
- 
## Classes

### NotificationInfo
The `NotificationInfo` class serves as the central hub for event-related data. It captures, manages, and provides access 
to information about due dates, notification periods, and notification statuses for various events.
#### Attributes
- **due_dates**: A dictionary mapping event names (strings) to their respective due dates (datetime objects). 
This provides a clear lookup of when an event is due to occur.
`due_dates: Dict[str, datetime]`
- **notification_periods**: A dictionary where each event name (string) maps to a list of integers. 
These integers represent the days before the due date when notifications should be sent out.
`notification_periods: Dict[str, List[int]]`
- **notification_status**: This dictionary holds the current status of notifications for each event. 
The statuses can be one of the following:

  * `SENT`: The notification has been dispatched.
  * `NOT_SENT`: No notification has been sent yet.
  * `ACK`: The notification was acknowledged by the user.
  * `DONE`: The event related to the notification is completed.
`notification_status: Dict[str, NotificationStatus]`
#### Constants
- CONFIG_DATE_FORMAT: A string constant that provides a unified date format for the system. 
It is used to ensure date consistency across the application.
`CONFIG_DATE_FORMAT = "%m/%d/%Y"`
#### Nested Classes
NotificationStatus Enum: This enumeration provides a structured and meaningful set of status values that can be assigned
to notifications.
```python
from enum import Enum, auto
class NotificationStatus(Enum):
    SENT = auto()
    NOT_SENT = auto()
    ACK = auto()
    DONE = auto()
```

### NotificationSender
The `NotificationSender` class is an abstract class that serves as a template for all notification handling classes.
```python
from abc import ABC, abstractmethod

class NotificationSender(ABC):

    @abstractmethod
    def send_notification(self, message: str):
        pass
```

### MailHandler (Under Development)
The MailHandler class inherits from NotificationSender and is intended to handle email notifications when fully developed.
### PushOverNotificationSender
The PushOverNotificationSender class also inherits from NotificationSender. It's responsible for sending notifications via the Pushover service.
### EventReminder
The main class `EventReminder` encapsulates both `DateHandler` and `NotificationHandler` and is responsible for the overall logic.
### EventReminderGUIElements 
as a GUI interface for your EventReminder system and includes a QLineEdit for entering an event name, another QLineEdit pre-filled with today's date as the due date, and two buttons for adding an event and checking notifications.
### Usage
To use this code, initialize the `EventReminder` class with your preferred notification handler and then use the add_event and remove_event methods to manage your events.
#### Example

```python
from event_reminder import EventReminder
from notification_info import NotificationInfo
from pushover_notification import PushOverNotificationSender
from event_reminder_GUI_elements import EventReminderGUIElements
from PyQt5.QtWidgets import QApplication
import sys


app = QApplication(sys.argv)

# Create EventReminder and DateHandler instances (replace these lines as needed)
# Initialize DateHandler and EventReminder
notification_info_holder = NotificationInfo()
notification_sender = PushOverNotificationSender("config.json")
event_reminder = EventReminder(notification_sender, notification_info_holder)

ex = EventReminderGUIElements(event_reminder)
ex.setWindowTitle('Event Reminder')
ex.resize(300, 200)
ex.show()

sys.exit(app.exec_())
```
## Scheduler Settings
To run a Python script at regular intervals, such as every hour, on a Windows 11 Pro system, you can utilize the Windows Task Scheduler. Here's a step-by-step guide to setting up a scheduled task:

1. Search for Task Scheduler:

   * Press the Windows key or click on the Start menu.
   * Type "Task Scheduler" and open it.

2. Create a New Task:
   * In the Actions Pane on the right, click "Create Basic Task".
3. Name and Describe Your Task:

   *   Give your task a name, e.g., "Run main.py hourly".
   *   Add a description if you want.
   *   Click "Next".
   * 
4. Specify the Trigger:
   * Choose the "Daily" trigger and click "Next".
   * Specify the start day and time.
   * For "Recur every", enter "1" days.
   * Click "Next".
   * Now, choose "Repeat task every" and select "1 hour" from the dropdown. For the duration, choose "1 day".
   * Click "Next".
5. Specify the Action:
   *   Choose "Start a program" and click "Next".
   *   In the "Program/script" field, browse and select your Python executable. Typically, this would be something like `C:\Users\<YourUsername>\AppData\Local\Programs\Python\<PythonVersion>\python.exe`. Adjust the path based on where Python is installed on your machine.
   *   In the "Add arguments (optional)" field, type the path to your `main.py` file, e.g., `C:\path\to\your\script\main.py`.
   *   You can leave the "Start in (optional)" field empty or add the directory of your main.py if required.
6. Finish the Setup:
   *   Click "Next", review your settings, and then click "Finish".

That's it! The Task Scheduler will now execute main.py using Python every hour.

**Note**: Make sure that the path to your Python interpreter and the path to your main.py script are correct. It's also good practice to test the task once by right-clicking on it in the Task Scheduler library and choosing "Run" to ensure everything works as expected.
