from abc import ABC, abstractmethod
import smtplib
from email.message import EmailMessage
from typing import Dict
from json_file_handler import JSONFileHandler

from sendgrid import SendGridAPIClient
import os
from sendgrid.helpers.mail import Mail, Email, To, Content

known_smtps = {'gmail': 'smtp.gmail.com', 'hotmail': 'smtp.hotmail.com', 'uama': 'smtp.sendgrid.net'}


class NotificationSender(ABC):
    json_file_handler: JSONFileHandler

    @abstractmethod
    def send_notification(self, message: str) -> bool:
        """
        :param message:
        :return: whether the notification is sent properly or not
        """
        pass


class MailSender(NotificationSender):
    notification_mail_ares: str
    sender_mail_ares: str
    known_smtps: Dict[str, str]
    smtp_provider: str

    def __init__(self, from_email_address: str, sent_to_email_address: str, json_file_path: str,
                 smtp_provider: str = None):
        self.sent_to_email_address = sent_to_email_address
        self.from_email_address = from_email_address
        self.json_file_handler = JSONFileHandler(json_file_path)
        self.password = self.json_file_handler.read_by_key("email_password")
        self.known_smtps = known_smtps
        if smtp_provider is None:
            smtp_tag = self.from_email_address.rsplit("@", 1)[-1].rsplit(".", 1)[0]
            if smtp_tag in self.known_smtps:
                self.smtp_provider = self.known_smtps[smtp_tag]
        else:
            self.smtp_provider = smtp_provider

    # function
    def create_smtp_with_tls(self, sender_smtp: str = 'smtp.gmail.com'):
        # Replace 'smtp.your_email_provider.com' and 587 with your SMTP server details
        server = smtplib.SMTP(sender_smtp, 587)
        server.starttls()
        return server  # Comment this line if your email provider doesn't require TLS

    def create_smtp_with_ssl(self, sender_smtp: str = 'smtp.gmail.com'):
        return smtplib.SMTP_SSL('smtp.your_email_provider.com', 465)

    def send_notification_via_sendgrid(self, message: str):
        subject = "Event Reminder"
        email_message = Mail(
            from_email=self.from_email_address,
            to_emails=self.sent_to_email_address,
            subject=subject,
            plain_text_content=message
        )

        try:
            # Replace "your_sendgrid_api_key_here" with your actual SendGrid API Key
            sg = SendGridAPIClient('your_sendgrid_api_key_here')
            response = sg.send(email_message)
            print("Email sent!")
        except Exception as e:
            print(f"An error occurred: {e}")

    def send_notification(self, message: str):
        msg = EmailMessage()
        msg.set_content(message)
        msg['Subject'] = 'Event Reminder'
        msg['From'] = self.from_email_address
        msg['To'] = self.sent_to_email_address
        server = self.create_smtp_with_ssl(self.smtp_provider)  # self.create_smtp_with_tls(self.smtp_provider)
        try:
            server.login(self.from_email_address, self.password)
            server.send_message(msg)
        except Exception as e:
            print(f"An error occurred: {e}")
        finally:
            server.quit()


if __name__ == "__main__":
    from_email_addr = "ilchicago_notifier@uama.us"
    mail_to_addr = "muhammetpakyurek@gmail.com"
    json_file_path = "config.json"
    email_handler = MailSender(from_email_addr, mail_to_addr, json_file_path)
    email_handler.send_notification_via_sendgrid("hello")
