import json
from typing import Any, Dict
from pathlib import Path
import logging
class JSONFileHandler:
    config_file_path = Path
    info_file_path = Path
    def __init__(self, config_file_path: str):
        if isinstance(config_file_path, str):
            config_file_path = Path(config_file_path)
        self.config_file_path = config_file_path
        self.info_file_path = config_file_path
        self.info_file_path = self.info_file_path.with_stem(config_file_path.stem + "_info")

    def read_by_key(self, key: str) -> Any:
        try:
            with open(self.config_file_path, 'r') as f:
                data = json.load(f)
                return data.get(key)
        except FileNotFoundError:
            logging.error("Config file not found")
        except json.JSONDecodeError:
            logging.error("Error decoding JSON from config file")

    def write(self, key: str, value: Any):
        try:
            with open(self.info_file_path, 'r') as f:
                data = json.load(f)
        except (FileNotFoundError, json.JSONDecodeError):
            data = {}
        data[key] = value
        with open(self.info_file_path, 'w') as f:
            json.dump(data, f)

    def remove_by_key(self, key:str):
        b_key_removed = False
        try:
            with open(self.info_file_path, 'r') as f:
                data = json.load(f)
        except (FileNotFoundError, json.JSONDecodeError):
            data = {}
        if key in data:
            del data[key]
            b_key_removed = True
        with open(self.info_file_path, 'w') as f:
            json.dump(data, f)
        return b_key_removed

    @staticmethod
    def read_all(file_path) -> Dict:
        try:
            with open(file_path, 'r') as f:
                data = json.load(f)
                return data
        except FileNotFoundError:
            logging.error("Config file not found")
            return {}
        except json.JSONDecodeError:
            logging.error("Error decoding JSON from config file")
            return {}

    @staticmethod
    def flush_file(file_path):
        with open(file_path, 'w') as f:
            json.dump({}, f)
