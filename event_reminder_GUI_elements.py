from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt
from datetime import datetime

from event_reminder import EventReminder
from notification_info import NotificationInfo, CONFIG_DATE_FORMAT
from pushover_notification import PushOverNotificationSender

class EventReminderGUIElements(QWidget):

    def __init__(self, event_reminder):
        super(EventReminderGUIElements, self).__init__()
        self.event_reminder = event_reminder
        self.initUI()

    def initUI(self):
        self.setWindowIcon(QIcon("logo_square.png"))

        hbox1_layout = QHBoxLayout()
        # Create a QLabel and set a pixmap (your big icon image) to it
        label = QLabel(self)
        pixmap = QPixmap('logo_square.png')
        resized_pixmap  = pixmap.scaled(100, 100, Qt.KeepAspectRatio)
        label.setPixmap(resized_pixmap)
        # Align the pixmap to the top-left corner of the QLabel
        #label.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        hbox1_layout.addWidget(label)
        vbox_layout = QVBoxLayout()
        self.event_name_entry = QLineEdit(self)
        self.event_name_entry.setPlaceholderText("Enter Event Name")
        vbox_layout.addWidget(self.event_name_entry)

        self.due_date_entry = QLineEdit(self)
        today = datetime.now().strftime(CONFIG_DATE_FORMAT)  # Get today's date
        self.due_date_entry.setText(today)
        self.due_date_entry.setPlaceholderText("Enter Due Date (YYYY-MM-DD)")
        vbox_layout.addWidget(self.due_date_entry)

        add_event_button = QPushButton('Add Event', self)
        add_event_button.clicked.connect(self.add_event)
        vbox_layout.addWidget(add_event_button)

        check_notifications_button = QPushButton('Check Notifications', self)
        check_notifications_button.clicked.connect(self.check_notifications)
        vbox_layout.addWidget(check_notifications_button)

        hbox1_layout.addLayout(vbox_layout)
        self.setLayout(hbox1_layout)

    def add_event(self):
        event_name = self.event_name_entry.text()
        due_date_str = self.due_date_entry.text()

        try:
            due_date = datetime.strptime(due_date_str, '%Y-%m-%d')
        except ValueError:
            QMessageBox.critical(self, 'Error', 'Invalid date format. Use YYYY-MM-DD.', QMessageBox.Ok)
            return

        if self.event_reminder.add_event_reminder(event_name, due_date):
            QMessageBox.information(self, 'Success', f'Event {event_name} added with due date {due_date}',
                                    QMessageBox.Ok)
        else:
            QMessageBox.critical(self, 'Error', f'Failed to add {event_name}  with due date {due_date}',
                                 QMessageBox.Ok)
            return


    def check_notifications(self):
        # Assuming your EventReminder class has a method that returns a list of due events.
        notifications = self.event_reminder.check_for_notifications()
        if notifications:
            QMessageBox.information(self, 'Notifications', f'{notifications}', QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'No Notifications', 'No upcoming events.', QMessageBox.Ok)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    # Create EventReminder and DateHandler instances (replace these lines as needed)
    # Initialize DateHandler and EventReminder
    notification_info_holder = NotificationInfo()
    notification_sender = PushOverNotificationSender("config.json")
    event_reminder = EventReminder(notification_sender, notification_info_holder)

    ex = EventReminderGUIElements(event_reminder)
    ex.setWindowTitle('Event Reminder')
    ex.resize(300, 200)
    ex.show()

    sys.exit(app.exec_())



